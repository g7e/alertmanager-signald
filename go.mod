module gitlab.com/thefinn93/alertmanager-signald

go 1.15

require (
	github.com/prometheus/alertmanager v0.23.0
	github.com/prometheus/client_golang v1.12.1
	gitlab.com/signald/signald-go v0.0.0-20220320054431-7ff54921568d
)
