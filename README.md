# Alert Manager Signald

A webhook receiver for Prometheus's Alert Manager to send to a Signal group via signald.


# Running

Set environment variables to specify the signald account to use and the group to send to.
The account much be already registered with the local signald, and much be in the group.

```
SIGNAL_USERNAME="+12024561414" SIGNAL_GROUP="AAAAAAAAAA00==" ./alertmanager-signald
```

Or, use this sytemd unit file by placing it at `/etc/systemd/system/alertmanager-signald`.
Make sure to put the `alertmanager-signald` program in the correct place, or update the
path in the unit file.

```
[Unit]
Description=Prometheus Alert Manager Signald
Wants=network.target
After=signald.service

[Service]
Type=simple
User=nobody
Group=signald
ExecStart=/usr/local/bin/alertmanager-signald
Environment=SIGNAL_USERNAME="+12024561414"
Environment=SIGNAL_GROUP="AAAAAAAAAA00=="
Restart=on-failure

[Install]
WantedBy=multi-user.target
```
