package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/prometheus/alertmanager/template"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/signald/signald-go/signald"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

var (
	SignaldAccountUsername = os.Getenv("SIGNAL_USERNAME")
	SignalGroup            = os.Getenv("SIGNAL_GROUP")
)

func main() {
	http.HandleFunc("/", index)
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/webhook", webhook)
	log.Fatal(http.ListenAndServe(":9121", nil))
}

func index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprint(w, "check out <a href=\"/metrics\">/metrics</a>")
}

func webhook(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body := template.Data{}
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		log.Fatalf("Error processing alerts: %s", err)
		return
	}
	log.Printf("received %d alert", len(body.Alerts))

	for _, alert := range body.Alerts {
		message := ""
		if summary, ok := alert.Annotations["summary"]; ok {
			message = fmt.Sprintf("%s\n\n", summary)
		}
		message = fmt.Sprintf("%s\nstatus: %s", message, alert.Status)
		for key, value := range alert.Labels {
			message = fmt.Sprintf("%s\n%s: %s", message, key, value)
		}
		message = fmt.Sprintf("%s\n\n%s\n", message, alert.GeneratorURL)
		log.Println(message)
		sendToSignal(message)
	}
	w.WriteHeader(http.StatusNoContent)
}

func sendToSignal(message string) {
	conn := signald.Signald{}
	if err := conn.Connect(); err != nil {
		log.Fatal(err)
	}
	defer conn.Close()
	req := v1.SendRequest{
		Username:         SignaldAccountUsername,
		RecipientGroupID: SignalGroup,
		MessageBody:      message,
	}
	_, err := req.Submit(&conn)
	if err != nil {
		log.Fatal("Failed to send request to socket", err)
	}
}
